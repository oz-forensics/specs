Pod::Spec.new do |s|
  s.name = 'OZLivenessSDK'
  s.version = '2.2.3'
  s.summary = 'OZLivenessSDK'
  s.homepage = 'https://gitlab.com/oz-forensics/oz-liveness-ios'
  s.authors = { 'oz-forensics' => 'info@ozforensics.ru' }
  s.source = { :git => 'https://gitlab.com/oz-forensics/oz-liveness-ios', :tag => s.version }
  s.vendored_frameworks = 'OZLivenessSDK.xcframework'
  s.ios.deployment_target  = '10.0'
  s.resources = 'OZLivenessSDKResources.bundle'

  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64 x86_64 i386' }
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64 x86_64 i386' }
  s.static_framework = true

  s.dependency 'TensorFlowLiteC', '2.4.0'

end
