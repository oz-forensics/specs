Pod::Spec.new do |s|
  s.name = 'OZLivenessSDK'
  s.version = '6.4.5'
  s.summary = 'OZLivenessSDK'
  s.homepage = 'https://gitlab.com/oz-forensics/oz-liveness-ios'
  s.authors = { 'oz-forensics' => 'info@ozforensics.ru' }
  s.source = { :git => 'https://gitlab.com/oz-forensics/oz-liveness-ios', :branch => 'dynamic'}
  s.ios.deployment_target  = '11.0'
  s.default_subspec = 'Full'

  s.subspec 'Full' do |ss|
    ss.vendored_frameworks = 'OZLivenessSDK.xcframework'
    ss.resources = ['OZLivenessSDKResources.bundle', 'OZLivenessSDKOnDeviceResources.bundle']
  end
  
  s.subspec 'Core' do |ss|
    ss.vendored_frameworks = 'OZLivenessSDK.xcframework'
    ss.resources = 'OZLivenessSDKResources.bundle'
  end
  
  s.dependency 'TensorFlowLiteC', '2.9.1'

end

