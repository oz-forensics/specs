Pod::Spec.new do |s|
  s.name = 'OZLivenessSDK'
  s.version = '3.0.2'
  s.summary = 'OZLivenessSDK'
  s.homepage = 'https://gitlab.com/oz-forensics/oz-liveness-ios'
  s.authors = { 'oz-forensics' => 'info@ozforensics.ru' }
  s.source = { :git => 'https://gitlab.com/oz-forensics/oz-liveness-ios' }
  s.ios.deployment_target  = '10.0'
  s.default_subspec = 'Full'

  s.subspec 'Full' do |ss|
    ss.vendored_frameworks = 'OZLivenessSDK.xcframework'
    ss.resources = ['OZLivenessSDKResources.bundle', 'OZLivenessSDKOnDeviceResources.bundle']
  end
  
  s.subspec 'Core' do |ss|
    ss.vendored_frameworks = 'OZLivenessSDK.xcframework'
    ss.resources = 'OZLivenessSDKResources.bundle'
  end
  
  s.dependency 'TensorFlowLiteC', '2.7.0'

end


